
#Tools Used
Backend - Laravel
Front End - Bootstrap 5 & Vanilla JS

#Approach
- In my approach, I implemented a UI to enable uploading of invoices as PDFs.
- Through this simple UI, the file is sent to the database and stored.
- Thereafter, I utilized pdfparser provided by Laravel which enables extraction of text from PDFs.

#Limitations
- Though I retrieved the texts from invoices, I didn't complete extraction of specific details as required.